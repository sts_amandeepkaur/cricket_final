from django.urls import path
from cricketapp import views
app_name='cricketapp'
urlpatterns = [
    path('scoreboard/',views.scoreboard,name='scoreboard'),
    path('player_profile/',views.player_profile,name='profile'),
    path('squad/',views.squad,name='squad'),
    path('search/',views.search,name='search'),
    path('upcoming/',views.upcoming,name='upcoming'),
    path('old_matches/',views.old_matches,name='old_matches'),
    path('contact/',views.contact,name='contact'),
    path('news/',views.newsV,name='news'),
    path('video/',views.video,name='video'),
     path('teams/',views.teamss,name='teams'),
]