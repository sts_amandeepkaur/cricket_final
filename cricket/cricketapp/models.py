from django.db import models

class feedback(models.Model):
    name = models.CharField(max_length=250)
    email = models.CharField(max_length=250)
    message = models.TextField()

    def __str__(self):
        return self.name

class news(models.Model):
    title = models.CharField(max_length=300) 
    content = models.CharField(max_length=1000)
    cover = models.ImageField(upload_to='news/%Y/%m/%d',null=True,blank=True)
    date = models.DateTimeField(auto_now_add=True)
    
    
    def __str__(self):
        return self.title

class teams(models.Model):
    name = models.CharField(max_length=300)
    captain = models.CharField(max_length=200)
    coach = models.CharField(max_length=200)

    def __str__(self):
        return self.name