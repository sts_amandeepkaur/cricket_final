from django.contrib import admin
from cricketapp.models import feedback
from cricketapp.models import news
from cricketapp.models import teams

admin.site.register(feedback)
admin.site.register(news)
admin.site.register(teams)